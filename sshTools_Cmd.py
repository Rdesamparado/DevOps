#!/usr/bin/env python
from find_ip import find_ip
import subprocess
import paramiko
import sys 
from helper import get_m_nodes, fs_webtoolkit_and_models_access, get_all_attributes
fs_webtoolkit_and_models_access()
from sshTools_Class import Servicenowinfo, Robloxinfo, Cumulusinfo
import time
from Artlib.ColorTerminalOutput import ColorTerminalOutput

#Credentials
Cumulus_User = 'cumulus'
Cumulus_PW = 'CumulusLinux!'
Raritan_User = 'admin'
Raritan_PW = 'P@ssw0rd'
Roblox_User = 'admn'
Roblox_PW = '#Roblox2020'
Test_User = 'piston'
Test_PW = 'cylinder'
Test_IP = '10.10.64.90'
Terminal = ColorTerminalOutput

def choice(Device_node):
    print ''
    print "Type \"usage\" below to see usage again."
    action = raw_input("Type your Selection [Number] here: ")
    print ''
    connect_(Device_node,action)

def pduportassign(Device_node):
    print 'PDU Port Assignment'
    Device_Fnder = [x.encode('ascii') for x in Device_node.app_name]
    node_project_ = [x.encode('ascii') for x in Device_node.project]
    for x in node_project_:
        node_project = str(x).strip(x[1])
    else:
        print ''
    node_ip = [x.encode('ascii') for x in Device_node.node_ip]
    for x in node_ip:
        node_ip_ = str(x).strip(str(['']))
    else:
        print ''
#    print node_ip_, Device_Fnder
    if 'PDU-B1' in Device_Fnder:
        cmd = 'python PDUPORTASSIGN.py %s %s 1'%(node_project,node_ip_)
        print cmd
        'PDU Port Assign started for PDU-B1'
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
        time.sleep(2)
        print output
        choice(Device_node)
    elif 'PDU-B2' in Device_Fnder:
        cmd = str('python PDUPORTASSIGN.py %s %s 2'%(node_project,node_ip_))
        print 'PDU Port Assign started for PDU-B2'
        print cmd
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
        time.sleep(2)
        print output
        choice(Device_node)
    else:
        print 'Unable to execute port assignment.'
        choice(Device_node)

def connect_(Device_node,action):
    sesh = paramiko.SSHClient()
    sesh.load_system_host_keys()
    sesh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    node_ip_ = [x.encode('ascii') for x in Device_node.node_ip]
    node_user = [x.encode('ascii') for x in Device_node.user]
    node_pw = [x.encode('ascii') for x in Device_node.pw]
    Dvice = [x.encode('ascii') for x in Device_node.manufacturer]
    Modelx = [x.encode('ascii') for x in Device_node.model]
    if action == 'usage':
        #Modelx = [x.encode('ascii') for x in Device_node.model]
        if Dvice == ['Servertech']:
            print Robloxinfo()
            choice(Device_node)
        elif Dvice == ['Raritan Computer']:    
            print Servicenowinfo()
            choice(Device_node)
        elif Dvice == ['Raritan']:
            print Servicenowinfo()
            choice(Device_node)
        elif Modelx == ['210-AEDQ']:
            print Cumulusinfo()
            choice(Device_node)
        else: 
            print "Model not available in System"
            choice(Device_node)

    if action == '12':
        sesh.connect('10.10.64.90', 22, 'piston','cylinder')
        print 'Test Server'
        channel = sesh.invoke_shell()
        time.sleep(5)
        test = raw_input("1 or 2: ")
        if test == '1':
            time.sleep(3)
            channel.send('ls | grep -i Docker\n')
            time.sleep(3)
            output = channel.recv(100000)
            for line in output.split('\n'):
                print (line)
            choice(Device_node)
        elif test == '2':
            time.sleep(3)
            channel.send('cd /etc/ansible/playbooks\n')
            time.sleep(3)
            channel.send('ls\n')
            time.sleep(3)
            output = channel.recv(100000)
    	    for line in output.split('\n'):
                print (line)
            choice(Device_node)
    	print ''
        choice()

    elif action == '1' and 'Raritan Computer' in Dvice or action == '1' and 'Raritan' in Dvice:
        pduportassign(Device_node)

    elif action == '1' and 'Raritan Computer' not in Dvice or action == '1' and 'Raritan' not in Dvice:
        print "\033[91mUnable to execute pduportassign on this device.\033[0m"
        choice(Device_node)

    elif len(action) == 1 and action == '2' or action == '3':
        for x in node_ip_:
            node_ip2 = str(x).strip(str(['']))
        for x in node_user:
            node_user_1 = str(x).strip(str(['']))
        for x in node_pw:
            node_pw_1 = str(x).strip(str(['']))
        print node_ip2,node_user_1,node_pw_1
        seshconnect = sesh.connect(node_ip2, 22, node_user_1, node_pw_1, disable_certificate_verification = True)
        print seshconnect
        #sesh.connect(Test_IP, 22, Test_User, Test_PW)
        channel = sesh.invoke_shell()
        print ''
        print "\033[92m[Connection Made]\033[0m"
        print ''
        time.sleep(3)
        Dvice = [x.encode('ascii') for x in Device_node.manufacturer]
        serial = [x.encode('ascii') for x in Device_node.serial]
        for x in serial:
            serial_ = str(x).strip(str(['']))
        print 'Device', str(Dvice),'\n'
        Snowmodel = ('Raritan')
        if Snowmodel not in str(Dvice): 
            print "\033[91mAction selected is for ServiceNow Raritan Devices.\033[0m"
            choice(Device_node)
        
        if action == '2' and Snowmodel in str(Dvice):# or action == '2' and Dvice == 'Raritan':
            print ''
            print 'Servicenow PDU Configuration'
            channel.send('config\n')
            time.sleep(3)
            channel.send('pdu name PDU-%s\n' %(serial_))
            print 'PDU-Serial Naming'
            time.sleep(3)
            channel.send('apply\n')
            time.sleep(3)
            channel.send('network services snmp v1/v2c enable\n')
            print 'Enabling SNMP'
            time.sleep(3)
            channel.send('network ipv4 interface ETH1 preferredHostName PDU-%s\n' %(serial_))
            print 'Registering Hostname'
            time.sleep(1)
            channel.send('show pdu details\n')
            print 'Generating Validation Output'
            channel.send('show network services snmp\n')
            time.sleep(3)
            output = channel.recv(10000)
            for line in output.split('\n'):
                print (line)
            time.sleep(3)
            print ''
            choice(Device_node)
        
        elif action == '3' and Snowmodel in str(Dvice):
            print ''
            print 'Collecting Servicenow PDU Logs'
            print '-'*110
            time.sleep(3)
            channel.send('show pdu details\n')
            time.sleep(2)
            channel.send('show network services snmp\n')
            time.sleep(2)
            channel.send('show network\n')
            time.sleep(2)
            channel.send('show outlets\n')
            time.sleep(8)
            output = channel.recv(100000)
            for line in output.split('\n'):
                print (line)
            print ''
            choice(Device_node)

    elif action == '4' or action == '5' or action == '6' or action == '7':
        #sesh.connect(Test_IP, 22, Test_User, Test_PW)
        for x in node_ip_:
            node_ip2 = str(x).strip(str(['']))
        for x in node_user:    
            node_user_1 = str(x).strip(str(['']))
        for x in node_pw:
            node_pw_1 = str(x).strip(str(['']))
        print node_ip2,node_user_1,node_pw_1
        seshconnect = sesh.connect(node_ip2, 22, node_user_1, node_pw_1)
        print seshconnect
        channel = sesh.invoke_shell()
        print "******channel******"
        print ''
        print "\033[92mConnection Made\033[0m "
        print ''
        Dvice = [x.encode('ascii') for x in Device_node.manufacturer]
        #Dmodel = [x.encode('ascii') for x in Device_node.model]
        time.sleep(3)
    
        if 'Servertech' not in Dvice:# and '210-AEDQ' in Dmodel:
            print "\033[91mAction selected is for Roblox Servertech Devices.\033[0m "
            choice(Device_node)
        
        elif action == '4' and 'Servertech' in Dvice:
            print '\033[4mConfigure Settings:\033[0m'
            print ''
            ip = raw_input("Input IP Address: ")
            subnet = raw_input("Input Subnet Mask: ")
            gateway = raw_input("Input Gateway: ")
            banner = raw_input("Input Banner & Location Name: ")
            channel.send('set ipv4 address\n')
            settings = [ip,subnet,gateway,banner]
            for line in settings:
                print line.strip('\n')
            Resp = raw_input("Type 'yes' to continue? ").lower()

            try:
                if str(Resp) == 'yes' or str(Resp) == 'y':
                    print settings
                    print "Setting ip, subnet & gateway"
                    time.sleep(1)
                    channel.send(ip + '\n')
                    time.sleep(1)
                    channel.send('set ipv4 subnet\n')
                    time.sleep(1)
                    channel.send(subnet + '\n')
                    time.sleep(1)
                    channel.send('set ipv4 gateway\n')
                    time.sleep(1) 
                    channel.send(gateway + '\n')
                    time.sleep(1)
                    print 'Setting FQDN and Location Name'
                    channel.send('set location\n')
                    time.sleep(1)
                    channel.send(banner + '\n')
                    time.sleep(1)
                    channel.send('set dhcp fqdn name\n')
                    time.sleep(1)
                    channel.send(banner + '\n')
                    time.sleep(1) 
                    channel.send('set snmp v2 enabled\n')
                    time.sleep(1)
                    channel.send('y\n')
                    time.sleep(1)
                    channel.send('set banner\n')
                    time.sleep(1)
                    channel.send(banner + '\n')
                    time.sleep(1)
                    channel.send('\x1a\n')
                    time.sleep(1)
                    channel.send('show system\n')
                    time.sleep(2)
                    channel.send('show units\n')
                    time.sleep(2)
                    channel.send('show network\n')
                    time.sleep(2)
                    channel.send('show outlets\n')
                    time.sleep(8)
                    output = channel.recv(100000)
                    for line in output.split('\n'):
                        print (line)
                    time.sleep(2)
                    restart = raw_input("Type \"Yes\" to restart device to apply changes: ")
                    if restart == "Yes" or restart == 'yes' or restart == 'y':
                        channel.send('restart\n')
                        time.sleep(1)
                        print 'restart'
                        channel.send('y\n')
                        print 'yes'
                        time.sleep(4)
                        output = channel.recv(100000)
                        for line in output.split('\n'):
                            print (line)
                        print 'Device Restarted.'
                        choice(Device_node)
                    elif restart == None:
                        print 'No action was made.'
                        choice(Device_node)
                    else:
                        print 'No action was made.'
                        choice(Device_node)
            except:
                choice(Device_node)

        elif action == '5':
            print 'Collecting Roblox PDU Logs'
            print '-' * 100
            channel.send('show system\n')
            time.sleep(2)
            channel.send('show units\n')
            time.sleep(2)
            channel.send('show network\n')
            time.sleep(2)
            channel.send('show outlets\n')
            time.sleep(8)
            output = channel.recv(100000)
            for line in output.split('\n'):
                print (line)
            choice(Device_node)

        elif action == '6' and 'Servertech' in Dvice:
            print 'Disabling DHCP'
            print "Connection will be lost"
            channel.send('set dhcp disabled\n')
            time.sleep(1)
            print 'Restarting Device'
            channel.send('restart\n')
            time.sleep(1)
            conf = raw_input('Type "yes" to continue:').lower()
            if conf == 'yes' or conf == 'y':
                channel.send('y\n')
                time.sleep(2)
                output = channel.recv(100000)
                for line in output.split('\n'):
                    print (line)
                choice(Device_node)
            else:
                channel.send('n\n')
                print 'No action was made'
                choice(Device_node)

        elif action == '7' and 'Servertech' in Dvice:
            print 'Restarting Device to Factory Defaults'
            channel.send('restart factory \n')
            conf = raw_input("Type 'yes' to continue: ").lower()
            if conf == 'yes' or conf == 'y':
                channel.send('y\n')
                time.sleep(2)
                output = channel.recv(1000000)
                for line in output.split('\n'):
                    print (line)
                choice(Device_node)
            else:
                channel.send('n\n')
                print 'No action was made'
                choice(Device_node)
        else:
            choice(Device_node)

    elif action == '8' or action == '9' or action == '10':
        for x in node_ip_:
            node_ip2 = str(x).strip(str(['']))
        for x in node_user:
            node_user_1 = str(x).strip(str(['']))
        for x in node_pw:
            node_pw_1 = str(x).strip(str(['']))
        print node_ip2,node_user_1,node_pw_1
        sesh.connect(str(node_ip2), 22, node_user_1,node_pw_1)
        #sesh.connect(Test_IP, 22, Test_User, Test_PW)
        channel = sesh.invoke_shell()
        print ''
        print "\033[92mConnection Made\033[0m "
        print ''
        Dmodel = [x.encode('ascii') for x in Device_node.model]
        time.sleep(3)

        if '210-AEDQ' not in Dmodel:
            print "\033[91mAction selected is for Dell Cumulus Devices!\033[0m "
            choice(Device_node)

        elif action == '8' and '210-AEDQ' in Dmodel:
            print '\033[4mAutomate Configurations:\033[0m'
            channel.send('sudo cl-license-i\n')
            time.sleep(1)
            channel.send(node_pw_1 + '\n')
            time.sleep(3)
            channel.send('fabien.broquet@servicenow.com|4CtjMCACDi5ctakKPHDBMa+EfmOVAg4IIO33JXhBpDZSERL11Q\n')
            time.sleep(1)
            channel.send('net add interface swp1-52\n')
            time.sleep(1)
            channel.send('net add bridge bridge ports swp1-52\n')
            time.sleep(1)
            channel.send('net commit\n')
            time.sleep(1)
            channel.send('sudo ifreload -a\n')
            time.sleep(1)
            channel.send(node_pw_1 + '\n')
            time.sleep(3)
            channel.send('net add interface swp1-52 alias [DCS TEST]\n')
            time.sleep(1)
            channel.send('net commit\n')
            time.sleep(1)
            channel.send('sudo systemctl restart switchd\n')
            time.sleep(1)
            channel.send(node_pw_1 + '\n')
            time.sleep(3)
            channel.send('sudo adduser root sudo\n')
            time.sleep(1)
            channel.send(node_pw_1 + '\n')
            time.sleep(2)
            channel.send('sudo passwd root\n')
            time.sleep(2)
            channel.send(node_pw_1 + '\n')
            time.sleep(2)
            output = channel.recv(1000000)
            for line in output.split('\n'):
                print (line)
            choice(Device_node)

        elif action == '9' and '210-AEDQ' in Dmodel:
            print 'Collecting Logs'
            channel.send('cat /etc/os-release\n')
            time.sleep(1)
            channel.send('decode-syseeprom\n')
            time.sleep(1)
            channel.send('cat /etc/network/interfaces\n')
            time.sleep(5)
            channel.send('ethtool -m swp49 | grep -E "Vendor PN|Vendor SN"\n')
            time.sleep(1)
            channel.send('ethtool -m swp50 | grep -E "Vendor PN|Vendor SN"\n')
            time.sleep(1)
            channel.send('ethtool -m swp51 | grep -E "Vendor PN|Vendor SN"\n')
            time.sleep(1)
            channel.send('ethtool -m swp52 | grep -E "Vendor PN|Vendor SN"\n')
            time.sleep(1)
            output = channel.recv(1000000)
            for line in output.split('\n'):
                print (line)
            choice(Device_node)
	
        elif action == '10' and '210-AEDQ' in Dmodel:
            print 'Automate Post Snapshot Configurations:'
            channel.send('net del all\n')
            time.sleep(1)
            channel.send('net commit\n')
            time.sleep(1)
            channel.send('sudo systemctl restart switchd\n')
            time.sleep(1)
            channel.send(node_pw_1 + '\n')
            time.sleep(3)
            channel.send('cat /etc/network/interfaces\n')
            time.sleep(5)
            channel.send('net add interface swp1-52 alias [DCS TEST]\n')
            time.sleep(1)
            channel.send('net commit\n')
            time.sleep(1)
            channel.send('sudo systemctl restart switchd\n')
            time.sleep(1)
            channel.send(node_pw_1 + '\n')
            time.sleep(3)
            channel.send('sudo adduser root sudo\n')
            time.sleep(1)
            channel.send(node_pw_1 + '\n')
            time.sleep(2)
            channel.send('sudo passwd root\n')
            time.sleep(2)
            channel.send(node_pw_1 + '\n')
            time.sleep(2)
            output = channel.recv(1000000)
            for line in output.split('\n'):
                print (line)
            choice(Device_node)

    else:
        raise Exception('Selection not available.')
        


if __name__ == '__main__':
    len_argv = len(sys.argv)
    m_nodes = None
    if len_argv < 2:
        print '<Node ID>,<Node ID> must be entered as an argument.'
        sys.exit(0)
    if len_argv == 2:
        #print usage()
        m_node = get_m_nodes(sys.argv[1])
        get_all = get_all_attributes(m_node[0])
        app_name = get_all['app_name']
        project = get_all['rack_number']

        for node in m_node:
            node_ip = find_ip(node.dhcp_macaddr)
            serial = node.serial_number
            print project,serial,node_ip,app_name
            try:
                if len(node_ip) <= 0:
                    print 'Unable to retrieve ip for this node.'
                    sys.exit(0)
                if len(node_ip) >= 0:
                    choice(serial, node_ip, app_name, project)
            except Exception as ex:
                print "%s" % ex

    else:
        print "<Node ID>,<Node ID> must be entered as an argument."
        sys.exit(1)
    

