#from helper import get_m_nodes, fs_webtoolkit_and_models_access #get_all_attributes
#fs_webtoolkit_and_models_access()
#import sys
from fs_razor_core.models import ManagedNode#,Customers
from find_ip import find_ip
from Artlib.ColorTerminalOutput import ColorTerminalOutput

Terminal = ColorTerminalOutput
my_node = ManagedNode
ServiceNow_usage = """


\t==========================================================================


\t\033[36m[ServiceNow PDU Configuration]\033[0m 

\t[1] Port Assignment

\t[2] Automate Configurations:
\t    - PDU-Serial Naming, Enabling SNMP and Register Hostname

\t[3] Collect Logs


\t==========================================================================


"""
Roblox_usage = """


\t==========================================================================


\t\033[36m[Roblox PDU Configuration]\033[0m

\t[4] Configure Settings:
\t    - Configure IP, Subnet Mask, Gateway.
\t    - Automate SNMP Enable, Configure Banner, Location & FQDN Name.

\t[5] Collect Logs

\t[6] Disabling DHCP
\t    - Note: Connectivity will be lost!

\t[7] Restart Device to Factory Defaults


\t==========================================================================


"""
Cumulus_usage = """

\t==========================================================================


\t\033[36m[Dell Cumulus Configuration]\033[0m

\t[8] Automate Configurations:
\t    - Applying License, Add Ports, Port Naming, Set Bridging and Add Users.

\t[9] Collect Logs

\t[10] Automate Post Snapshot Configurations:
\t    - Delete all configs then re-automate configs without Bridging.


\t==========================================================================


"""

def Servicenowinfo():
    print ServiceNow_usage

def Robloxinfo():
    print Roblox_usage

def Cumulusinfo():
    print Cumulus_usage


class Device:
    def __init__(self, m_nodes):
        self.m_nodes = m_nodes
        self.device_type = []
        self.serial = []
        self.node_ip = []
        self.app_name = []
        self.project = []
        self.manufacturer = []
        self.model = []
        self.customer_ = []
        self.user = []
        self.pw = []
        self.sku = []
        self.result = None
        self.device_type_nodes = []
        self.nodes_ids = []
        self.set_nodes()
        self.port = 22

    def set_nodes(self):
	Snow_Dell_User = 'cumulus'
	Snow_Dell_PW = 'CumulusLinux!'
	Snow_Pdu_User = 'admin'
	Snow_Pdu_PW = 'P@ssw0rd'
        Snow_JNPR_User = 'root'
        Snow_JNPR_PW = ''
	Roblox_User = 'admn'
	Roblox_PW = '#Roblox2020'
        Square_User = 'admn'
        Square_PW = 'admn'

        for m_node in self.m_nodes:
            #attributes = m_node.attributes.filter(attribute_name__in=['sku','device_type','app_name','serialnumber',
                                                               # 'rack_number','customer.name','manufacturer','model'])
            attributes = m_node.attributes.filter()
            properties = {}
            for attr in attributes:
                properties[attr.attribute_name] = attr.value
            m_node.sku = properties['sku']
            m_node.device_type = properties['device_type']
            m_node.app_name = properties['app_name']
            m_node.serial = properties['serialnumber']
            m_node.project = properties['rack_number']
            try:
                m_node.node_ip = find_ip(m_node.dhcp_macaddr)
                if len(m_node.node_ip) == 0:
                    m_node.node_ip = ('\033[91mno ip found (check connection)\033[0m')
            except:
                m_node.node_ip = 'no ip'
            m_node.manufacturer = properties['manufacturer']
            try:
                m_node.model = properties['model']
                Roblox_pdu = ['C2W42CE-YQME2M00/C','C2X42CE-YQME2M00/C','C1W24VT-5PPA17AC/C',
                              'C1X24VT-5PPA17AC/C', 'C1L24VS-4PFA11F2/C','C1S24VS-4PFA11F2/C']
                Square_pdu = ['C2W42CE-DQME2M00/0C.7ft.red','C2X42CE-DQME2M00/0C.7ft.blue']
                Snow_pdu = ['PX3-5961I2U-C8V2A6K1','PX3-5785U-V2A6K2','PX3-5961I2U-C8V2A6K2',
                            'PX3-5961I2U-V2A6K2','PX3-5703-A6K1','PX3-5723I2-C8A6K2','PX3-5723I2-C8A6K1']
        	Snow_dell_swi = ['210-AEDQ']
                Snow_jnpr_swi = ['QFX5100-48S-AFI','QFX5120-48Y-AFI']
                AllServers = ['c6420','r640','R740']
                Square_swi = ['DCS-7050TX2-128-R']
                if m_node.model in (Square_pdu):
                    m_node.user = Square_User
                    m_node.pw = Square_PW
                elif m_node.model in (Roblox_pdu):
                    m_node.user = Roblox_User
                    m_node.pw = Roblox_PW
                elif m_node.model in (Snow_pdu):
                    m_node.user = Snow_Pdu_User
                    m_node.pw = Snow_Pdu_PW
                elif m_node.model in (Snow_dell_swi):
                    m_node.user = Snow_Dell_User
                    m_node.pw = Snow_Dell_PW
                elif m_node.model in (Snow_jnpr_swi):
                    m_node.user = Snow_JNPR_User
                    m_node.pw = Snow_JNPR_PW
                elif m_node.model in (AllServers):
                    m_node.user = 'root'
                    m_node.pw = 'fr3sca'
                elif m_node.model in (Square_swi):
                    m_node.user = 'admin'
                    m_node.pw = ''
            except:
                m_node.model = 'no model'

            self.sku.append(m_node.sku)
            self.device_type.append(m_node.device_type)
            self.serial.append(m_node.serial)
            self.app_name.append(m_node.app_name)
            self.project.append(m_node.project)
            self.device_type_nodes.append(m_node)
            self.nodes_ids.append(m_node.id)
            self.node_ip.append(m_node.node_ip)
            self.project.append(m_node.custom_flag)
            self.customer_.append(m_node.customer.name)
            self.manufacturer.append(m_node.manufacturer)
            self.model.append(m_node.model)                
            self.user.append(m_node.user)
            self.pw.append(m_node.pw)
