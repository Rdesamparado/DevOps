import sys 
from helper import get_m_nodes, fs_webtoolkit_and_models_access 
from sshTools_Class import Device, Servicenowinfo, Robloxinfo, Cumulusinfo
from sshTools_Cmd import choice
fs_webtoolkit_and_models_access()
#from fs_razor_core.models import ManagedNode

def Info_(Device_node):
    manu_ = [x.encode('ascii') for x in Device_node.manufacturer]
    #customer = Device_node.customer_
    #for x in customer:
     #   customer_ = x.encode('ascii')
    model = Device_node.model
    for x in model:
        model_ = x.encode('ascii')
    app_name = Device_node.app_name
    for x in app_name:
        app_name_ = x.encode('ascii')
    sku = Device_node.sku
    for x in sku:
        sku_ = x.encode('ascii')
    User = Device_node.user
    for x in User:
        User_ = x.encode('ascii')
    PW = Device_node.pw
    for x in PW:
        PW_ = x.encode('ascii')
    node_ip = Device_node.node_ip
    for x in node_ip:
        node_ip_ = x.encode('ascii')
    device_type = Device_node.device_type
    for x in device_type:
        device_type_ = x.encode('ascii')
    serial = Device_node.serial
    for x in serial:
        serial_ =  x.encode('ascii')
    project = Device_node.project
    for x in project:
        project = x.encode('ascii')
   
    Vendor_List = ['Raritan Computer','Servertech','Arista']
    Inventory = ["DCS-7050TX2-128-R","210-AEDQ"]
    if 'Arista' in Vendor_List:
        print '-' * 110
        #print '',str(customer_)
        print "This is a %s %s Device for %s" %(manu_,device_type_,project)
        print "SN: %s | App_Name: %s | Model: %s" %(serial_,app_name_ ,model_)        
        print "ip: %s | Login: %s | PW: %s | SKU: %s" %(node_ip_,User_,PW_,sku_)
	if "Servertech" in manu_:
            print Robloxinfo()
        elif "Raritan Computer" in manu_:
            print Servicenowinfo()
        elif "210-AEDQ" in model_:
            print Cumulusinfo() 
        elif "DCS-7050TX2-128-R" in model:
            print Robloxinfo()

    elif ["DCS-7050TX2-128-R"] in Inventory:
        print '-'*110
        print ''
        print "This is a %s %s Device for %s" %(manu_,device_type_, project)
        print "SN: %s | App_Name: %s | Model: %s" %(serial_,app_name_ ,model_)
        print "ip: %s | Login: %s | PW: %s | SKU: %s" %(node_ip_,User_,PW_,sku_)
        print Robloxinfo()
    
    else:
        print "Model Device not available in system."
        sys.exit(0) 
    choice(Device_node)

if __name__ == '__main__':
    len_argv = len(sys.argv)
    if len_argv == 2:
        m_nodes = get_m_nodes(sys.argv[1])
    else:
        print "<Node ID>,<Node ID> must be entered as an argument."
        sys.exit(1)
    
    Device_node = Device(m_nodes)
    Info_(Device_node)


